const render = require('react-dom').render
const h = require('react-hyperscript')
const configureStore = require('./lib/store')
const Root = require('./app/root.js')
const Eth = require('ethjs');
const fetch = require('node-fetch');
const ZeroEx = require('0x.js').ZeroEx;
let BigNumber = require('bignumber.js');


let web3Found = false
window.addEventListener('load', function() {
  var sendButton = document.getElementById('send');
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    // Use Mist/MetaMask's provider
    web3Found = true
    const Web3 = require('web3');
    console.log("web3 is " + typeof web3);
  } else {
    // Fall back to Infura:
    web3Found = false
    eth = new Eth(new Eth.HttpProvider('https://mainnet.infura.io'))
    // TODO: do things
  }
});
  // Now you can start your app & access web3 freely:
  // start the magic when the form is submitted
window.addEventListener('click', function() { 
  // Number of decimals to use (for ETH and ZRX)
  const DECIMALS = 18;

  var amount = document.getElementById("amountToSend").value;
  console.log("amount is" + amount)
  var address = document.getElementById("addressToSend").value;
  var e = document.getElementById("coinSelect");
  var tokenName = e.options[e.selectedIndex].text;

  // (async () => {
  // switch (tokenName) {
  //   case "OMG":
  //     var tokenID = "omisego";
  //     break;
  //   case "REP":
  //     var tokenID = "augur";
  //     break;
  //   case "GNO":
  //     var tokenID = "gnosis-gno";
  //     break;
  //   case "QTUM":
  //     var tokenID = "qtum";
  //     break;
  //   case "BAT":
  //     var tokenID = "basic-attention-token"
  //     break;
  // }
  // if (tokenName == 'SAI') {

  //ran out of time to get this API call working


    var usdPrice = new BigNumber(1.0);



  // } else {
  // var usdPrice = await new BigNumber(coinmarketcap.tickerByAsset(tokenID)[0].price_usd);
  // }
// });

  let DESTINATION_ADDRESS = address.toLowerCase();
  let bigAmount = new BigNumber((amount), DECIMALS);
  let tradeAmount = bigAmount.dividedBy(usdPrice);

  web3 = new Web3(web3.currentProvider);
  console.log(web3)
  window.web3 = web3;
  const zeroEx = new ZeroEx(web3.currentProvider);
  console.log(zeroEx);


  (async () => {

      //const NetworkID = web3.version.network;

    // console.log("ID is " + networkID);
      const NULL_ADDRESS = ZeroEx.NULL_ADDRESS; 
      // let ZRX_ADDRESS  = await zeroEx.exchange.getZRXTokenAddressAsync();        // The ZRX token contract
      // console.log(ZRX_ADDRESS);
      let EXCHANGE_ADDRESS = await zeroEx.exchange.getContractAddressAsync();  // The Exchange.sol address (0x exchange smart contract)
      console.log(EXCHANGE_ADDRESS);

      // SAI Kovan token address = 0x228BF3D5BE3ee4b80718b89b68069b023c32131E
      const SAI_ADDRESS = "0x228BF3D5BE3ee4b80718b89b68069b023c32131E".toLowerCase();;
      // MKR Kovan address = 0x1dad4783cf3fe3085c1426157ab175a6119a04ba
      const MKR_ADDRESS = "0x1dad4783cf3fe3085c1426157ab175a6119a04ba";     
    
      const ownerAddress = await zeroEx.getAvailableAddressesAsync() + "";
      console.log("user account is" + ownerAddress);
      const makerAddress = ownerAddress
    
      // Unlimited allowances to 0x contract for maker and taker
      const txHashAllowMaker = await zeroEx.token.setUnlimitedProxyAllowanceAsync(MKR_ADDRESS,  makerAddress); 
      await zeroEx.awaitTransactionMinedAsync(txHashAllowMaker);
      console.log('Maker Allowance Mined...');
    
      // const txHashAllowTaker = await zeroEx.token.setUnlimitedProxyAllowanceAsync(SAI_ADDRESS, takerAddress);
      // await zeroEx.awaitTransactionMinedAsync(txHashAllowTaker);
      // console.log('takerFee Allowance Mined...');

      // Deposit WETH
      // const ethToConvert = ZeroEx.toBaseUnitAmount(new BigNumber(1), DECIMALS); // Number of ETH to convert to WETH

      // const txHashWETH = await zeroEx.etherToken.depositAsync(ethToConvert, takerAddress);
      // await zeroEx.awaitTransactionMinedAsync(txHashWETH);
      // console.log('ETH -> WETH Mined...');

      // Generate order
      console.log('Generating order')
      let order = {
          maker: makerAddress,
          taker: NULL_ADDRESS,
                    feeRecipient: NULL_ADDRESS,
                    makerTokenAddress: MKR_ADDRESS,
                      takerTokenAddress: SAI_ADDRESS,
                      exchangeContractAddress: EXCHANGE_ADDRESS,
                      salt: ZeroEx.generatePseudoRandomSalt(),
                    makerFee: new BigNumber(0),
                      takerFee: new BigNumber(0),
                    makerTokenAmount: ZeroEx.toBaseUnitAmount(new BigNumber(0.1), DECIMALS),  // Base 18 decimals
                    takerTokenAmount: ZeroEx.toBaseUnitAmount(new BigNumber(0.2), DECIMALS),  // Base 18 decimals
                    expirationUnixTimestampSec: new BigNumber(Date.now() + 3600000)        // Valid up to an hour
                    };

      // Create orderHash
      console.log(order);
      let orderHash = ZeroEx.getOrderHashHex(order);
      console.log("orderHash is " + orderHash)
      console.log(typeof orderHash);
      console.log(typeof makerAddress);
      console.log(makerAddress);

      // Signing orderHash -> ecSignature
      let ecSignature = await zeroEx.signOrderHashAsync(orderHash, makerAddress);
      console.log("ecSignature is " + ecSignature);

      // Appending signature to order
      const signedOrder = {
                ...order,
                ecSignature,
                          };

      // Verify if order is fillable
      console.log("signed order made")
      let txHash = await zeroEx.exchange.validateOrderFillableOrThrowAsync(signedOrder);

      // // Try to fill order
      // const shouldThrowOnInsufficientBalanceOrAllowance = true;
      // const fillTakerTokenAmount = ZeroEx.toBaseUnitAmount(new BigNumber(0.1), DECIMALS);

      // // Try filling order
      // console.log("about to fill order");
      // const txHash = await zeroEx.exchange.fillOrderAsync(signedOrder, fillTakerTokenAmount,
      //                                                           shouldThrowOnInsufficientBalanceOrAllowance, takerAddress);

      // Transaction Receipt
      const txReceipt = await zeroEx.awaitTransactionMinedAsync(txHash);
      console.log(txReceipt.logs);

      // Send Sai on to Destination Address
      let amount = ZeroEx.toBaseUnitAmount(new BigNumber(0.2), DECIMALS)
      console.log("Amount of Sai in Wei being sent to DESTINATION: " + amount)
      const txHash2 = await zeroEx.token.transferAsync(SAI_ADDRESS, makerAddress, DESTINATION_ADDRESS, amount);

    })().catch(console.log);
}, false);

